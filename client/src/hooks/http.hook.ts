import { useState, useCallback, useContext } from 'react'
import { useHistory } from 'react-router-dom'
import { AuthContext } from '../context/AuthContext'

// eslint-disable-next-line import/prefer-default-export
export const useHttp = () => {
  const [loading, setLoading] = useState(false)
  const [error, setError] = useState(null)
  const history = useHistory()
  const { logout } = useContext(AuthContext)

  const request = useCallback(async (url, method = 'GET', body = null, headers = {}) => {
    setLoading(true)
    let newBody = body
    const newHeaders = headers
    try {
      if (body) {
        newBody = JSON.stringify(body)
        newHeaders['Content-Type'] = 'application/json'
      }

      const response = await fetch(url, { method, body: newBody, headers: newHeaders })
      const data = await response.json()
      if (data.reason === 'jwt expired' || data.reason === 'invalid token') {
        logout()
        history.push({ pathname: '/auth', state: { from: history.location.pathname } })
      }

      if (!response.ok) {
        throw new Error(data.message || 'Что-то пошло не так')
      }

      setLoading(false)

      return data
    } catch (e) {
      setLoading(false)
      setError(e.message)
      throw e
    }
  }, [history, logout])

  const clearError = useCallback(() => setError(null), [])

  return {
    loading, request, error, clearError,
  }
}
