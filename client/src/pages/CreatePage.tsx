import React, { useContext, useState } from 'react'
import { useHistory } from 'react-router-dom'
import { useHttp } from '../hooks/http.hook'
import { AuthContext } from '../context/AuthContext'
import Grid from '../components/Grid'
import InputField from '../components/InputField'
import Container from '../components/Container'

const CreatePage = () => {
  const history = useHistory()
  const auth = useContext(AuthContext)
  const { request } = useHttp()
  const [link, setLink] = useState('')

  const pressHandler = async (event:React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key === 'Enter') {
      try {
        const data = await request('/api/link/generate', 'POST', { from: link }, {
          Authorization: `Bearer ${auth.token}`,
        })
        history.push(`/detail/${data.link._id}`)
      // eslint-disable-next-line no-empty
      } catch (e) {}
    }
  }

  return (
    <Container>
      <Grid container>
        <Grid xs={8} style={{ paddingTop: '2rem' }}>
          <InputField
            fullWidth
            label="Введите ссылку"
            inputProps={{
              placeholder: 'Вставьте ссылку',
              id: 'link',
              type: 'text',
              value: link,
              onChange: (e) => setLink(e.target.value),
              onKeyPress: pressHandler,
            }}
          />
        </Grid>
      </Grid>
    </Container>
  )
}
export default CreatePage
