import React, {
  useCallback, useContext, useEffect, useState,
} from 'react'
import { useHttp } from '../hooks/http.hook'
import { AuthContext } from '../context/AuthContext'
import Loader from '../components/Loader'
import LinksList from '../components/LinksList'
import Container from '../components/Container'

const LinksPage = () => {
  const [links, setLinks] = useState([])
  const { loading, request } = useHttp()
  const { token } = useContext(AuthContext)

  const fetchLinks = useCallback(async () => {
    try {
      const fetched = await request('/api/link', 'GET', null, {
        Authorization: `Bearer ${token}`,
      })
      setLinks(fetched)
    // eslint-disable-next-line no-empty
    } catch (e) {}
  }, [token, request])

  useEffect(() => {
    fetchLinks()
  }, [fetchLinks])

  if (loading) {
    return <Loader />
  }

  return (
    <Container style={{ marginTop: 24 }}>
      {!loading && <LinksList links={links} />}
    </Container>
  )
}
export default LinksPage
