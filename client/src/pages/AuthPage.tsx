import React, { useContext, useState } from 'react'
import { useHistory } from 'react-router-dom'
import { useHttp } from '../hooks/http.hook'
import { AuthContext } from '../context/AuthContext'
import Grid from '../components/Grid'
import FormContainer from '../components/Auth/FormContainer'
import InputField from '../components/InputField'
import Button from '../components/Button'
import Container from '../components/Container'

const AuthPage = () => {
  const auth = useContext(AuthContext)
  const history = useHistory()
  const historyState = history.location.state as {from?:string} | undefined
  const { loading, request } = useHttp()
  const [form, setForm] = useState({
    email: '', password: '',
  })

  const changeHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
    setForm({ ...form, [event.target.name]: event.target.value })
  }

  const registerHandler = async () => {
    try {
      await request('/api/auth/register', 'POST', { ...form })
      // eslint-disable-next-line no-empty
    } catch (e) {}
  }

  const loginHandler = async () => {
    try {
      const data = await request('/api/auth/login', 'POST', { ...form })
      auth.login(data.token, data.userId)
      if (historyState?.from) history.push(historyState.from)
      else history.push('/create')
      // eslint-disable-next-line no-empty
    } catch (e) {}
  }

  return (
    <Container style={{
      height: 'calc(100vh - 64px)',
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
    }}
    >
      <Grid container>
        <Grid xs={12} sm={10} md={6} lg={4} xl={3}>
          <FormContainer>
            <InputField
              fullWidth
              label="Email"
              inputProps={{
                placeholder: 'Введите email',
                id: 'email',
                type: 'text',
                name: 'email',
                value: form.email,
                onChange: changeHandler,
              }}
            />
            <InputField
              fullWidth
              label="Пароль"
              inputProps={{
                placeholder: 'Введите пароль',
                id: 'password',
                type: 'password',
                name: 'password',
                value: form.password,
                onChange: changeHandler,
              }}
            />
            <Grid container fullWidth>
              <Button
                style={{ marginRight: 10 }}
                disabled={loading}
                onClick={loginHandler}
                variant="contained"
              >
                Войти
              </Button>
              <Button
                onClick={registerHandler}
                disabled={loading}
                variant="outlined"
              >
                Регистрация
              </Button>
            </Grid>
          </FormContainer>
        </Grid>
      </Grid>
    </Container>
  )
}
export default AuthPage
