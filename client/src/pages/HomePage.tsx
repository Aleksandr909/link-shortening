import * as React from 'react'
import Container from '../components/Container'
import HeaderImage from '../components/Home/HeaderImage'
import HeaderImageBottomShadow from '../components/Home/HeaderImageBottomShadow'
import Grid from '../components/Grid'
import SocialContainer from '../components/Home/SocialContainer'
import Link from '../components/Link'
import {
  Gmail, Telegram, Github, Gitlab,
} from '../core/constants/icons'
import Footer from '../components/Footer'

interface IHomePageProps {
}

const HomePage: React.FunctionComponent<IHomePageProps> = (props) => (
  <>
    <div style={{ maxHeight: '100vh', marginTop: -64, overflow: 'hidden' }}>
      <HeaderImage />
    </div>
    <HeaderImageBottomShadow />
    <Container>
      <section>
        <Grid container justify="center">
          <Grid
            xs={12}
            md={10}
            style={{
              border: '1px solid #888', borderRadius: 4, boxShadow: '2px 2px 4px #eee', padding: 24, margin: '24px 0',
            }}
          >
            <h2 style={{ textAlign: 'center' }}>О проекте</h2>
            <p>
              <b>
                Основа приложения взята из
                {' '}
                <a href="https://github.com/vladilenm/mern-full-course" target="_blank" rel="noreferrer">
                  открытоко искочника
                </a>
                {' '}
                <a href="https://www.youtube.com/watch?v=ivDjWYcKDZI" target="_blank" rel="noreferrer">
                  онлайн курса
                </a>
                {' '}
                <a href="https://vk.com/vladilen.minin" target="_blank" rel="noreferrer">
                  Владилена Минина
                </a>
              </b>
            </p>
            <p>
              Проект размещен на сервере
              {' '}
              <a href="https://vscale.io/" target="_blank" rel="noreferrer">
                Vscale
              </a>
              {' '}
              с Ubuntu OS. Настроено https (с помощью letsencrypt создан ssl сертификат).
              С помощью
              {' '}
              <a href="https://gitlab.com/Aleksandr909/link-shortening/-/blob/master/.gitlab-ci.yml" target="_blank" rel="noreferrer">
                gitlab ci
              </a>
              {' '}
              проект автоматически собирается на серверах gitlab и затем переносится на сервер.
              На сервере с помощью nginx приложение проксируется с порта на нужный поддомен.
            </p>
          </Grid>
        </Grid>
      </section>
      <section>
        <Grid container justify="center">
          <Grid xs={12} md={10}>
            <h2 style={{ textAlign: 'center' }}>Обновления</h2>
            <ol>
              <li>
                Добавлена возможность сокращения ссылки через api, передавая в query логин и пароль
              </li>
              <li>Переход с materialize на styled-components</li>
              <li>Добавлена темная тема</li>
              <li>Стилизация кнопок и инпутов</li>
              <li>Переход на typescript на фронте</li>
              <li>Настройка eslint-а</li>
              <li>Настройка gitlab-ci, сервера, nginx (проксирование, https и др.)</li>
              <li>Сделана страница с описанием проекта</li>
              <li>Рефакторинг backend-а на typescript</li>
              <li>Улучшение авторизации</li>
              <li>Придумана фавиконка</li>
            </ol>
          </Grid>
        </Grid>
      </section>
      <section>
        <Grid container justify="center">
          <Grid xs={12} md={10} style={{ maxWidth: '100%' }}>
            <h2 style={{ textAlign: 'center' }}>Локальное тестирование</h2>
            <div
              style={{
                border: '1px solid #888',
                borderRadius: 4,
                boxShadow: '2px 2px 4px rgba(140, 140, 140, 0.4)',
                padding: '0 16px',
                backgroundColor: 'rgba(238, 238, 238, 0.4)',
                marginBottom: 24,
              }}
            >
              <pre
                style={{
                  overflowX: 'auto', margin: 0,
                }}
              >
                <code>
                  <p id="LC1">
                    git clone https://gitlab.com/Aleksandr909/link-shortening.git
                  </p>
                  <p id="LC2">
                    cd link-shortening
                  </p>
                  <p id="LC3">
                    npm run load
                  </p>
                  <p id="LC4">
                    npm run dev
                  </p>
                </code>
              </pre>
            </div>
          </Grid>
        </Grid>
      </section>
    </Container>
    <Footer>
      <Container>
        <Grid container justify="space-between" alignItems="center">
          <div style={{ textTransform: 'uppercase' }}>
            <Link href="https://www.vorfolio.ru">Vorfolio</Link>
          </div>
          <SocialContainer>
            <a href="mailto: sasha2822222@gmail.com">
              <Gmail />
            </a>
            <a href="https://t.me/vordgi" target="_blank" rel="noopener noreferrer">
              <Telegram />
            </a>
            <a href="https://github.com/Aleksandr909" target="_blank" rel="noopener noreferrer">
              <Github />
            </a>
            <a href="https://gitlab.com/Aleksandr909" target="_blank" rel="noopener noreferrer">
              <Gitlab />
            </a>
          </SocialContainer>
        </Grid>
        <div style={{ marginTop: 16 }}>
          © 2020 Савельев Александр. Все права защищены.
        </div>
      </Container>
    </Footer>
  </>
)

export default HomePage
