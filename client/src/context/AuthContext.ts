import { createContext } from 'react'

function noop() {}

// eslint-disable-next-line import/prefer-default-export
export const AuthContext = createContext({
  token: null,
  userId: null,
  login: noop as (jwtToken: any, id: any) => void,
  logout: noop,
  isAuthenticated: false,
})
