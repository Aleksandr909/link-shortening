import { createContext } from 'react'

function noop() {}

// eslint-disable-next-line import/prefer-default-export
export const ThemeContext = createContext({
  darkMode: false,
  changeDarkMode: noop as () => void,
})
