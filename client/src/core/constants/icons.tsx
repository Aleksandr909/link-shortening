import { ReactComponent as Sun } from '../../img/ico/sun.svg'
import { ReactComponent as Moon } from '../../img/ico/moon.svg'
import { ReactComponent as Telegram } from '../../img/ico/telegram.svg'
import { ReactComponent as Gmail } from '../../img/ico/gmail.svg'
import { ReactComponent as Gitlab } from '../../img/ico/gitlab.svg'
import { ReactComponent as Github } from '../../img/ico/github.svg'
import { ReactComponent as ChevronLeft } from '../../img/ico/chevron-left.svg'

// eslint-disable-next-line import/prefer-default-export
export {
  Sun,
  Moon,
  Gmail,
  Github,
  Gitlab,
  Telegram,
  ChevronLeft,
}
