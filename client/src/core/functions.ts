/* eslint-disable import/prefer-default-export */
export const isWidthDown = (maxWidth: number) => window.innerWidth < maxWidth
export const getScreenType = () => {
  if (window.innerWidth < 540) return 'xs'
  if (window.innerWidth < 960) return 'sm'
  return 'lg'
}
