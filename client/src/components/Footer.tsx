import styled from 'styled-components'

const Footer = styled.footer`
    padding: 24px 0;
    background-color: ${({ theme }) => `rgba(${theme.colorsRGB.antiBody[theme.style]}, 0.2)`}
`

export default Footer
