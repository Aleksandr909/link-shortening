import styled from 'styled-components'

interface ITableProps {
    fullWidth?: boolean
}

const Table = styled.table<ITableProps>`
    border-spacing: 0;
    border-collapse: separate;
    width: ${({ fullWidth }) => (fullWidth ? '100%' : 'fit-content')};
    & > thead > tr > th {
        text-align: left;
        border-top: ${({ theme }) => `1px solid ${theme.colors.antiBody[theme.style]}`};
        border-bottom: ${({ theme }) => `1px solid ${theme.colors.antiBody[theme.style]}`};
    };
    & > tbody > tr {
        padding: 12px 0;
        cursor: pointer;
        &:hover {
            background-color: ${({ theme }) => `rgba(${theme.colorsRGB.antiBody[theme.style]}, 0.2)`};
        }
        & > td {
            text-align: left;
            border-bottom: ${({ theme }) => `1px solid rgba(${theme.colorsRGB.antiBody[theme.style]}, 0.5)`};
        };
    }
`

export default Table
