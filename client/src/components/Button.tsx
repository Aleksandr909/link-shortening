import React, { useRef } from 'react'
import styled from 'styled-components'

interface IButtonProps extends React.ButtonHTMLAttributes<HTMLButtonElement>{
    styleColor?: string
    variant?: 'contained' | 'outlined'
}

const Button = styled(({
  styleColor, children, variant, ...buttonProps
}: IButtonProps) => {
  const animBtn = useRef<null | HTMLButtonElement>(null)
  React.useEffect(() => {
    const element = animBtn.current
    if (element) {
      let ELEMENTS_SPAN: any = ''
      const addAnimation = false
      if (!ELEMENTS_SPAN) ELEMENTS_SPAN = element.querySelector('span') as HTMLSpanElement
      element.addEventListener('mouseover', (e) => {
        ELEMENTS_SPAN.style.left = `${e.pageX - element.offsetLeft}px`
        ELEMENTS_SPAN.style.top = `${e.pageY - element.offsetTop}px`
        if (addAnimation) element.classList.add('animated')
      })
      element.addEventListener('mouseout', (e) => {
        ELEMENTS_SPAN.style.left = `${e.pageX - element.offsetLeft}px`
        ELEMENTS_SPAN.style.top = `${e.pageY - element.offsetTop}px`
      })
    }
  }, [])
  return (
    <button type="button" {...buttonProps} ref={animBtn}>
      <span />
      <p>{children}</p>
    </button>
  )
})`
    outline: 0;
    display: flex;
    align-items: center;
    --width: 100%;
    --time: 0.7s;
    cursor: pointer;
    position: relative;
    height: 32px;
    padding: 0 20px;
    color: ${({ variant, theme }) => (variant === 'outlined' ? theme.colors.primary[theme.style] : theme.colors.antiTextPrimary[theme.style])};
    background: ${({ variant, theme }) => (variant === 'contained' ? theme.colors.primary[theme.style] : 'none')};
    border: ${({ variant, theme }) => (variant === 'outlined' ? `1px solid ${theme.colors.primary[theme.style]}` : 0)};
    overflow: hidden;
    border-radius: 4px;
    transition: background 0.7s ease 0.1s;
    & p {
        z-index: 5;
        line-height: 1;
        margin: 0;
    };
    &:hover {
      & > span {
        width: calc(var(--width) * 2.5);
        padding-top: calc(var(--width) * 2.5);
      }
    };
    & span {
      border-radius: 100%;
      position: absolute;
      display: block;
      z-index: 0;
      width: 0;
      height: 0;
      background: #000;
      opacity: 0.2;
      transform: translate(-50%, -50%);
      transition: width var(--time), padding-top var(--time);
    };
    &.animated {
        --angle: 6deg;
        animation: shake 0.3s;
    };
    @keyframes shake {
        25%: {
        transform: rotate(calc(var(--angle) * -1));
        };
        50%: {
        transform: rotate(var(--angle));
        };
        100%: {
        transform: rotate(0deg);
        };
    };
`

export default Button
