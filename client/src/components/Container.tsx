import styled from 'styled-components'

const Container = styled.div`
    padding: 0 20px;
    margin: 0 auto;
    width: calc(100% - 40px);
    @media(min-width: 600px) {
        width: 600px;
    };
    @media(min-width: 960px) {
        width: 960px;
    };
    @media(min-width: 1280px) {
        width: 1280px;
    };
    @media(min-width: 1920px) {
        width: 1920px;
    }
`

export default Container
