import * as React from 'react'
import Label from './Label'
import Input from './Input'
import InputContainer from './InputContainer'

interface IInputFieldProps {
    label?: string;
    fullWidth?: boolean;
    inputProps?: React.InputHTMLAttributes<HTMLInputElement>;
    children?: React.ReactNode
}

const InputField: React.FunctionComponent<IInputFieldProps> = (
  { label, fullWidth, inputProps }:IInputFieldProps,
) => {
  const [isFocused, setIsFocused] = React.useState(false)
  const onFocus = (e:any) => {
    if (inputProps?.onFocus) inputProps.onFocus(e)
    setIsFocused(true)
  }
  const onBlur = (e: any) => {
    if (inputProps?.onBlur) inputProps.onBlur(e)
    setIsFocused(false)
  }

  return (
    <InputContainer fullWidth={fullWidth}>
      {label && <Label isFocused={isFocused}>{label}</Label>}
      <Input {...inputProps} onFocus={onFocus} onBlur={onBlur} />
    </InputContainer>
  )
}

export default InputField
