import styled from 'styled-components'

interface Props {
    isFocused: boolean
}
const Label = styled.label<Props>`
    display: block;
    font-size: 14px;
    width: fit-content;
    margin-left: 8px;
    z-index: 1;
    position: relative;
    color: ${({ isFocused, theme }) => isFocused && theme.colors.primary[theme.style]};
`

export default Label
