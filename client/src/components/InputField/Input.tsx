import * as React from 'react'
import styled from 'styled-components'

interface IInputProps extends React.HTMLProps<HTMLInputElement>{
}

const Input = styled(({ ...inputProps }: IInputProps) => <input {...inputProps} />)`
    border-radius: 2px;
    outline: none;
    border: 1px solid #888;
    height: 32px;
    padding-left: 8px;
    box-sizing: border-box;
    width: 100%;
    background-color: unset;
    color: ${({ theme }) => theme.colors.textPrimary[theme.style]};
    &:hover {
        border:  ${({ theme }) => `1px solid ${theme.colors.textPrimary[theme.style]}`};
    };
    &:focus {
        border:  ${({ theme }) => `1px solid ${theme.colors.primary[theme.style]}`};
    };
    ::placeholder {
        color: ${({ theme }) => theme.colors.textPrimaryTransparent[theme.style]};
    }
`

export default Input
