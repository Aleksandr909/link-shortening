import styled from 'styled-components'

interface IInputContainerProps{
    fullWidth?: boolean
}

const InputContainer = styled.div<IInputContainerProps>`
    position: relative;
    width: ${({ fullWidth }) => (fullWidth ? '100%' : 'fit-content')};
    margin: 8px 0;
`

export default InputContainer
