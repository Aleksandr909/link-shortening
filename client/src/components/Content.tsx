import styled from 'styled-components'

const Content = styled.div`
    min-height: 100vh;
    overflow: auto;
`

export default Content
