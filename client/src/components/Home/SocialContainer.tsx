import styled from 'styled-components'

const SocialContainer = styled.div`
    & > a > svg {
        width: 32px;
        height: 32px;
        transition: all 0.2s ease;
        fill: ${({ theme }) => theme.colors.textPrimary[theme.style]};
        @media(max-width: 600px) {
            width: 24px;
            height: 24px;
        };
    };
    &:hover {
        & > a > svg {
            fill: ${({ theme }) => theme.colors.textPrimary[theme.style]};
            opacity: 0.5;
        }
    };
    & > a {
        margin: 0 4px;
        & > svg {
            &:hover {
                fill: #D84315;
                opacity: 1;
                filter: drop-shadow(0 0 4px #E64A19);
            };
        }
    };
`

export default SocialContainer
