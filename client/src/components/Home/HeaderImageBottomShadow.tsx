import styled from 'styled-components'

const HeaderImageBottomShadow = styled.div`
    box-shadow: ${({ theme }) => `0 -4px 8px 12px ${theme.colors.body[theme.style]}`};
    position: relative;
`

export default HeaderImageBottomShadow
