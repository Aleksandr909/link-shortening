import styled from 'styled-components'
import { Header } from '../../core/constants/img'

const HeaderImage = styled(Header)`
    @media(min-width: 960px) {
        margin-top: -15%
    };
    & ~ div {
      box-shadow: ${({ theme }) => `0 -8px 8px 16px ${theme.colors.body[theme.style]}`};
      position: relative;
    }
`

export default HeaderImage
