import React from 'react'
import { useHistory } from 'react-router-dom'
import IconButton from './IconButton'
import { ChevronLeft } from '../core/constants/icons'
import Grid from './Grid'

interface ILink {
  to: string;
  from: string;
  clicks: string;
  date: string
}
interface ILinkCardProps {
  link: ILink
}

const LinkCard: React.FC<ILinkCardProps> = ({ link }:ILinkCardProps) => {
  const history = useHistory()
  const historyState = history.location.state as {from?:string} | undefined

  const onClickHandler = () => {
    if (historyState?.from) history.push(historyState.from)
  }

  return (
    <>
      <Grid container alignItems="center">
        {historyState?.from && (
          <IconButton style={{ marginRight: 16 }} onClick={onClickHandler}>
            <ChevronLeft />
          </IconButton>
        )}
        <h2>Ссылка</h2>
      </Grid>
      <p>
        Ваша ссылка:
        <a href={link.to} target="_blank" rel="noopener noreferrer">{link.to}</a>
      </p>
      <p>
        Откуда:
        <a href={link.from} target="_blank" rel="noopener noreferrer">{link.from}</a>
      </p>
      <p>
        Количество кликов по ссылке:
        <strong>{link.clicks}</strong>
      </p>
      <p>
        Дата создания:
        <strong>{new Date(link.date).toLocaleDateString()}</strong>
      </p>
    </>
  )
}
export default LinkCard
