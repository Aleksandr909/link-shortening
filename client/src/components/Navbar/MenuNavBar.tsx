import styled from 'styled-components'

const MenuNavBar = styled.div`
    z-index: 3;
    width: 30px;
    &, &:before, &:after{
        transition: transform .3s ease, background-color .3s ease .3s;
        height: 4px;
        border-radius: 2px;
        background-color: ${({ theme }) => theme.colors.body[theme.style]};
    }
    &:before, &:after {
        transition: transform .3s ease .3s, top .3s ease .3s, right .3s ease .3s, left .3s ease .3s, background-color .3s ease .3s;
        width: calc(50% + 2px);
        content: "";
        position: relative;
        top: -20px;
        display: inline-block;
    };
    &:before {
        right: calc(-50% + 2px);
    };
    &:after {
        left: 0;
    };
    &.opened {
        background-color: ${({ theme }) => theme.colors.textPrimary[theme.style]};
        transform: rotate(45deg);
        &:before {
            background-color: ${({ theme }) => theme.colors.textPrimary[theme.style]};
            top: -18px;
            right: -6px;
            transform: rotate(-90deg);
        };
        &:after {
            background-color: ${({ theme }) => theme.colors.textPrimary[theme.style]};
            top: -22px;
            left: 6px;
            transform: rotate(-90deg);
        };
    }
`

export default MenuNavBar
