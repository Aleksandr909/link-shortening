import * as React from 'react'
import { useHistory } from 'react-router-dom'
import MenuLink from '../MenuLink'
import { AuthContext } from '../../context/AuthContext'

interface INavListProps {
}

const NavList: React.FunctionComponent<INavListProps> = (props) => {
  const history = useHistory()
  const auth = React.useContext(AuthContext)

  const logoutHandler = (event: React.MouseEvent<HTMLAnchorElement>) => {
    event.preventDefault()
    auth.logout()
    history.push({ pathname: '/auth', state: { from: history.location.pathname } })
  }
  return (
    <ul>
      <li><MenuLink to="/create">Создать</MenuLink></li>
      <li><MenuLink to="/links">Ссылки</MenuLink></li>
      <li>
        <MenuLink
          to={{ pathname: '/auth', state: { from: history.location.pathname } }}
          onClick={logoutHandler}
        >
          Выйти
        </MenuLink>
      </li>
    </ul>
  )
}

export default NavList
