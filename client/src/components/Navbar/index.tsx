import React, { useContext } from 'react'
import { AuthContext } from '../../context/AuthContext'
import Container from '../Container'
import Grid from '../Grid'
import Header from './Header'
import { Sun, Moon } from '../../core/constants/icons'
import { ThemeContext } from '../../context/ThemeContext'
import IconButton from '../IconButton'
import MenuLink from '../MenuLink'
import HeaderBg from './HeaderBg'
import { isWidthDown } from '../../core/functions'
import MobileMenu from './MobileMenu'
import NavList from './NavList'

const Navbar = () => {
  const auth = useContext(AuthContext)
  const { darkMode, changeDarkMode } = useContext(ThemeContext)

  return (
    <Header>
      <HeaderBg />
      <Container>
        <Grid container justify="space-between" alignItems="center">
          <Grid container alignItems="center">
            <IconButton style={{ marginRight: 16 }} onClick={changeDarkMode}>
              {darkMode ? (
                <Moon style={{ color: 'inherit' }} />
              ) : (
                <Sun style={{ color: 'inherit' }} />
              )}
            </IconButton>
            <MenuLink to="/" style={{ zIndex: 1 }}>Shorelin</MenuLink>
          </Grid>
          {auth.isAuthenticated ? (
            <>
              {isWidthDown(960) ? (
                <MobileMenu />
              ) : (
                <NavList />
              )}
            </>
          ) : (
            <ul>
              <li><MenuLink to="/auth">Войти</MenuLink></li>
            </ul>
          )}
        </Grid>
      </Container>
    </Header>
  )
}
export default Navbar
