import styled from 'styled-components'

const HeaderSize = styled.div`
    position: relative;
    width: 100%;
    height: 64px;
`

export default HeaderSize
