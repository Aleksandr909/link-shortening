import styled from 'styled-components'

const HeaderBg = styled.div`
    position: fixed;
    width: 100%;
    height: 64px;
    background-color: ${({ theme }) => theme.colors.primary[theme.style]};
    opacity: 0.8;
`

export default HeaderBg
