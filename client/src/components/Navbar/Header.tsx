import styled from 'styled-components'

const Header = styled.nav`
    position: fixed;
    width: 100%;
    height: 64px;
    display: flex;
    align-items: center;
    color: ${({ theme }) => theme.colors.antiTextPrimary[theme.style]};
    text-transform: uppercase;
    box-shadow: 0 2px 4px #607D8B;
    z-index: 9999;
    & ul {
        list-style-type: none;
        margin: 0;
        padding: 0;
        & > li {
            display: inline;
            padding: 0 10px;
        }
    };
`

export default Header
