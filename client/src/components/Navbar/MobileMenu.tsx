import React, { useState } from 'react'
import MenuNavBar from './MenuNavBar'
import MenuNavBarContainer from './MenuNavBarContainer'
import MobileMenuContainer from './MobileMenuContainer'
import BackDrop from './BackDrop'
import HeaderSize from './HeaderSize'
import NavList from './NavList'

const MobileMenu = () => {
  const [isOpened, setIsOpened] = useState(false)

  const onClickHandler = () => {
    setIsOpened(!isOpened)
  }

  return (
    <>
      <BackDrop
        className={isOpened ? 'opened' : ''}
        onClick={() => setIsOpened(false)}
      />
      <MenuNavBarContainer onClick={onClickHandler}>
        <MenuNavBar className={isOpened ? 'opened' : ''} />
      </MenuNavBarContainer>
      <MobileMenuContainer className={isOpened ? 'opened' : ''}>
        <HeaderSize />
        <div style={{ padding: '0 16px' }}>
          <NavList />
        </div>
      </MobileMenuContainer>
    </>
  )
}
export default MobileMenu
