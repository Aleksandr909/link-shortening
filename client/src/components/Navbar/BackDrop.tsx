import styled from 'styled-components'

const BackDrop = styled.div`
    width: 100vw;
    height: 100vh;
    position: fixed;
    left: 0;
    top: 0;
    background: #000;
    opacity: 0;
    visibility: hidden;
    transition: opacity .3s ease;
    &.opened {
        opacity: 0.5;
        visibility: visible;
    }
`

export default BackDrop
