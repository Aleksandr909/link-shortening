import styled from 'styled-components'

const MenuNavBarContainer = styled.div`
    z-index: 3;
    height: 64px;
    display: flex;
    align-items: center;
    cursor: pointer;
`

export default MenuNavBarContainer
