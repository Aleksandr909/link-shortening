import styled from 'styled-components'

const MobileMenuContainer = styled.div`
    transition: all .5s ease;
    position: fixed;
    width: calc(100vw - 100px);
    height: 100vh;
    top: 0;
    right: calc(-100vw + 100px);
    background-color: ${({ theme }) => `rgba(${theme.colorsRGB.body[theme.style]},0.5)`};
    z-index: 2;
    &.opened {
        right: 0;
    };
    & li {
        display: block !important;
        margin: 24px 0;
        color: ${({ theme }) => theme.colors.textPrimary[theme.style]};
    }
`

export default MobileMenuContainer
