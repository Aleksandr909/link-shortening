import styled from 'styled-components'

const FormContainer = styled.div`
    padding: 16px;
    border-radius: 4px;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    background: linear-gradient(135deg, rgba(255, 255, 255, .05), rgba(0, 0, 0, .03));
    box-shadow: 2px 2px 6px #607D8B;
`

export default FormContainer
