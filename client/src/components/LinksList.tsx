import React from 'react'
import { useHistory } from 'react-router-dom'
import Table from './Table'

interface ILink {
  _id: string;
  to: string;
  from: string;
  clicks: string;
  date: string
}
interface ILinksListProps {
  links: ILink[]
}

const LinksList:React.FC<ILinksListProps> = ({ links }:ILinksListProps) => {
  const history = useHistory()
  if (!links.length) {
    return <p className="center">Ссылок пока нет</p>
  }
  return (
    <Table fullWidth>
      <thead>
        <tr>
          {/* eslint-disable-next-line jsx-a11y/control-has-associated-label */}
          <th />
          <th>Оригинальная</th>
          <th>Сокращенная</th>
        </tr>
      </thead>

      <tbody>
        { links.map((link, index) => (
          <tr
            key={link._id}
            onClick={
              () => history.push({ pathname: `/detail/${link._id}`, state: { from: '/links' } })
            }
          >
            <td>
              {index + 1}
              .
            </td>
            <td>{link.from}</td>
            <td>{link.to}</td>
          </tr>
        )) }
      </tbody>
    </Table>
  )
}
export default LinksList
