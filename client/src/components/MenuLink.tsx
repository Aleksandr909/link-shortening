import styled from 'styled-components'
import NavLinkStyled from './NavLinkStyled'

const MenuLink = styled(NavLinkStyled)`
    position: relative;
    &:after {
        content: "";
        height: 2px;
        position: absolute;
        left: 50%;
        bottom: -2px;
        width: 0;
        border-radius: 1px;
        background: ${({ theme }) => theme.colors.linkLine[theme.style]};
        transition: all .5s ease;
    }
    &:hover {
        &:after {
            left: 0;
            width: 100%;
        }
    }
`

export default MenuLink
