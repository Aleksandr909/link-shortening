import styled from 'styled-components'
import { NavLink } from 'react-router-dom'

const NavLinkStyled = styled(NavLink)`
    text-decoration: none;
    color: inherit;
`

export default NavLinkStyled
