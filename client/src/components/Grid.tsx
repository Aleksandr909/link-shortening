import styled from 'styled-components'

interface IGridProps {
    xs?: number,
    sm?: number,
    md?:number,
    lg?: number,
    xl?:number,
    container?: boolean,
    justify?: 'center' | 'flex-end' | 'flex-start' | 'space-between'
    alignItems?: 'center' | 'flex-end' | 'flex-start'
    fullWidth?: boolean
    direction?: 'column' | 'row'
}

const Grid = styled.div<IGridProps>`
    width: ${({ fullWidth }) => (fullWidth ? '100%' : 'auto')};
    display: ${({ container }) => (container ? 'flex' : 'block')};
    flex-wrap: wrap;
    flex-basis: ${({ xs }) => xs && `${(100 / 12) * xs}%`};
    justify-content: ${({ justify }) => justify};
    align-items: ${({ alignItems }) => alignItems};
    flex-direction: ${({ direction }) => direction};
    box-sizing: border-box;
    @media(min-width: 600px) {
        flex-basis: ${({ sm }) => sm && `${(100 / 12) * sm}%`};
    };
    @media(min-width: 960px) {
        flex-basis: ${({ md }) => md && `${(100 / 12) * md}%`};
    };
    @media(min-width: 1280px) {
        flex-basis: ${({ lg }) => lg && `${(100 / 12) * lg}%`};
    };
    @media(min-width: 1920px) {
        flex-basis: ${({ xl }) => xl && `${(100 / 12) * xl}%`};
    }
`

export default Grid
