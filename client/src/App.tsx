import React, { useState } from 'react'
import { BrowserRouter as Router } from 'react-router-dom'
import { ThemeProvider, createGlobalStyle } from 'styled-components'
import useRoutes from './routes'
import { useAuth } from './hooks/auth.hook'
import { AuthContext } from './context/AuthContext'
import Navbar from './components/Navbar'
import Loader from './components/Loader'
import Content from './components/Content'
import { ThemeContext } from './context/ThemeContext'

const themeObj = {
  colors: {
    body: { light: '#ffffff', dark: '#263238' },
    antiBody: { light: '#263238', dark: '#ffffff' },
    main: { light: 'blue', dark: 'white' },
    primary: { light: '#039BE5', dark: '#81D4FA' },
    primaryTransparent: { light: 'rgba(3, 155, 229, 0.3)', dark: 'rgba(129, 212, 250, 0.3)' },
    textPrimary: { light: '#000000', dark: '#ffffff' },
    textPrimaryTransparent: { light: 'rgba(0, 0, 0 , 0.5)', dark: 'rgba(255, 255, 255, 0.5)' },
    antiTextPrimary: { light: '#ffffff', dark: '#000000' },
    linkLine: { light: 'linear-gradient(90deg, #00B8D4, #00E5FF)', dark: 'linear-gradient(90deg, #84FFFF, #18FFFF)' },
  },
  colorsRGB: {
    body: { light: '255, 255, 255', dark: '38, 50, 56' },
    antiBody: { light: '38, 50, 56', dark: '255, 255, 255' },
    main: { light: 'blue', dark: 'white' },
    primary: { light: '#039BE5', dark: '#81D4FA' },
    primaryTransparent: { light: 'rgba(3, 155, 229, 0.3)', dark: 'rgba(129, 212, 250, 0.3)' },
    textPrimary: { light: '#000000', dark: '#ffffff' },
    textPrimaryTransparent: { light: 'rgba(0, 0, 0 , 0.5)', dark: 'rgba(255, 255, 255, 0.5)' },
    antiTextPrimary: { light: '#ffffff', dark: '#000000' },
    linkLine: { light: 'linear-gradient(90deg, #00B8D4, #00E5FF)', dark: 'linear-gradient(90deg, #84FFFF, #18FFFF)' },
  },
}

type Theme = typeof themeObj

declare module 'styled-components' {
  interface DefaultTheme extends Theme {
    style: 'light' | 'dark'
  }
}

const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
    color: ${({ theme }) => theme.colors.textPrimary[theme.style]};
    background-color: ${({ theme }) => theme.colors.body[theme.style]};
    width: 100vw;
    overflow-x: hidden;
  }
  #root {
    height: 100vh;
  }
  a {
    color: inherit
  }
`
function App() {
  const {
    token, login, logout, userId, ready,
  } = useAuth()
  const [darkMode, setDarkMode] = useState(JSON.parse(localStorage?.getItem('darkMode') || 'false'))
  const changeDarkMode = () => {
    setDarkMode(!darkMode)
    localStorage.setItem('darkMode', String(!darkMode))
  }
  const isAuthenticated = !!token
  const routes = useRoutes(isAuthenticated)

  if (!ready) {
    return <Loader />
  }

  return (
    <AuthContext.Provider value={{
      token, login, logout, userId, isAuthenticated,
    }}
    >
      <ThemeContext.Provider value={{ darkMode, changeDarkMode }}>
        <ThemeProvider theme={{ ...themeObj, style: darkMode ? 'dark' : 'light' }}>
          <GlobalStyle />
          <Router>
            <Navbar />
            <Content>
              <div style={{ height: 64 }} />
              {routes}
            </Content>
          </Router>
        </ThemeProvider>
      </ThemeContext.Provider>
    </AuthContext.Provider>
  )
}

export default App
