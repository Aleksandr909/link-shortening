# Shorelin

[![pipeline status](https://gitlab.com/Aleksandr909/link-shortening/badges/master/pipeline.svg)](https://gitlab.com/Aleksandr909/link-shortening/-/commits/master)

### [Приложение](https://shorelin.vorfolio.ru/)

**Основа приложения взята из [открытоко искочника](https://github.com/vladilenm/mern-full-course) [онлайн курса](https://www.youtube.com/watch?v=ivDjWYcKDZI) [Владилена Минина](https://vk.com/vladilen.minin)**

**Fullstack приложение для:**

1. Сокращения ссылок

### Обновления

1. Добавлена возможность сокращения ссылки через api, передавая в query логин и пароль
1. Переход с materialize на styled-components
1. Добавлена темная тема
1. Стилизация кнопок и инпутов
1. Переход на typescript на фронте
1. Настройка eslint-а
1. Настройка gitlab-ci, сервера, nginx (проксирование, https и др.)
1. Сделана страница с описанием проекта
1. Рефакторинг backend-а на typescript
1. Улучшение авторизации
1. Придумана фавиконка

### Планы

1. Улучшение api
1. Интернационализация

Проект размещен на сервере Vscale с Ubuntu OS. Настроено https (с помощью **letsencrypt** создан ssl сертификат). С помощью **[gitlab ci](https://gitlab.com/Aleksandr909/link-shortening/-/blob/master/.gitlab-ci.yml)** проект автоматически собирается на серверах gitlab и затем переносится на сервер. На сервере с помощью **nginx** приложение проксируется с порта на нужный поддомен.

### Локальное тестирование

```sh
git clone https://gitlab.com/Aleksandr909/link-shortening.git
cd link-shortening
npm run load
npm run dev
```

### Лицензия

[BSD 2-clause "Simplified" License](https://gitlab.com/Aleksandr909/link-shortening/-/blob/master/LICENSE)
