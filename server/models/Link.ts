import {
  Schema, model, Types, Document,
} from 'mongoose'

interface ILink {
  from: string,
  to: string,
  code: string,
  date: Date,
  clicks: number,
  owner?: string,
}

interface ILinkModel extends Document {
  _doc: ILink
}

const schema:Schema = new Schema({
  from: { type: String, required: true },
  to: { type: String, required: true, unique: true },
  code: { type: String, required: true, unique: true },
  date: { type: Date, default: Date.now },
  clicks: { type: Number, default: 0 },
  owner: { type: Types.ObjectId, ref: 'User' },
})

export default model<ILinkModel>('Link', schema)
