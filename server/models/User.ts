import {
  Schema, model, Types, Document,
} from 'mongoose'

interface IUser {
  email: string,
  password: string,
  links?: string,
}

interface IUserModel extends Document {
  _doc: IUser
}

const schema = new Schema({
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  links: [{ type: Types.ObjectId, ref: 'Link' }],
})

export default model<IUserModel>('User', schema)
