const { CUSTOM_CONFIG } = process.env

type CustomConfig = {
    jwtSecret: string
    mongoUri: string
}

const customConfig:CustomConfig = CUSTOM_CONFIG ? JSON.parse(CUSTOM_CONFIG) : {
  jwtSecret: '',
  mongoUri: '',
}

export default customConfig
