/* eslint-disable max-len */
import jwt from 'jsonwebtoken'
import { Response, Request } from 'express'
import customConfig from '../customConfig'

interface UserDecode {
  userId: string
}

export interface RequestWithUser extends Request {
  [key:string]: any
}
const authMiddleware = (req:RequestWithUser, res:Response, next:any) => {
  if (req.method === 'OPTIONS') {
    return next()
  }

  try {
    const token = req.headers.authorization?.split(' ')[1] // "Bearer TOKEN"
    if (!token) {
      return res.status(401).json({ message: 'Нет авторизации' })
    }

    const decoded = jwt.verify(token, customConfig.jwtSecret) as UserDecode
    req.currentUser = decoded
    next()
  } catch (e) {
    console.log(e.message)
    res.status(401).json({ reason: e.message, message: 'Нет авторизации' })
  }
}
export default authMiddleware
