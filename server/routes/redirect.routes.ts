import { Router, Response } from 'express'
import { Request } from 'express-validator/src/base'

import Link from '../models/Link'

const router = Router()

router.get('/:code', async (req:Request, res:Response) => {
  try {
    const link = await Link.findOne({ code: req.params?.code })

    if (link) {
      // eslint-disable-next-line no-plusplus
      link._doc.clicks++
      await link.save()
      return res.redirect(link._doc.from)
    }

    res.status(404).json('Ссылка не найдена')
  } catch (e) {
    res.status(500).json({ message: 'Что-то пошло не так, попробуйте снова' })
  }
})

module.exports = router
