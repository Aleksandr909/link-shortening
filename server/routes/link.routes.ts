import { Router, Response } from 'express'
import config from 'config'
import shortid from 'shortid'
import bcrypt from 'bcryptjs'
import auth, { RequestWithUser } from '../middleware/auth.middleware'
import User from '../models/User'
import Link from '../models/Link'

const router = Router()

router.post('/generate', auth, async (req:RequestWithUser, res:Response) => {
  try {
    const { login, password } = req.query as {[key:string]: string}
    const baseUrl = config.get('baseUrl')
    const { from } = req.body

    const code = shortid.generate()

    const existing = await Link.findOne({ from })

    if (existing) {
      return res.json({ link: existing })
    }

    const to = `${baseUrl}/l/${code}`
    if (login && password) {
      const user = await User.findOne({ login })
      if (!user) {
        return res.status(400).json({ message: 'Пользователь не найден' })
      }
      const isMatch = await bcrypt.compare(password, user._doc.password)
      if (!isMatch) {
        return res.status(400).json({ message: 'Неверный пароль' })
      }
      const link = new Link({
        code, to, from, owner: req.currentUser.userId,
      })
      await link.save()
      res.status(201).json({ link })
    } else {
      const link = new Link({
        code, to, from, owner: req.currentUser.userId,
      })
      await link.save()
      res.status(201).json({ link })
    }
  } catch (e) {
    res.status(500).json({ message: 'Что-то пошло не так, попробуйте снова' })
  }
})

router.get('/', auth, async (req:RequestWithUser, res:Response) => {
  try {
    const links = await Link.find({ owner: req.currentUser.userId })
    res.json(links)
  } catch (e) {
    res.status(500).json({ message: 'Что-то пошло не так, попробуйте снова' })
  }
})

router.get('/:id', auth, async (req:RequestWithUser, res:Response) => {
  try {
    const link = await Link.findById(req.params.id)
    res.json(link)
  } catch (e) {
    res.status(500).json({ message: 'Что-то пошло не так, попробуйте снова' })
  }
})

module.exports = router
